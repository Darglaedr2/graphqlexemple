package queries

import (
	"github.com/eko/graphql-go-structure/types"
	"github.com/graphql-go/graphql"
)

// GetUserQuery returns the queries available against user type.
func GetUserQuery() *graphql.Field {
	return &graphql.Field{
		Type: graphql.NewList(types.UserType),
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			var users []types.User
			users = append(users, types.User{
				ID:        1,
				Firstname: "Hoang",
				Lastname:  "CORNU",
			})
			users = append(users, types.User{
				ID: 2,
				Firstname: "John",
				Lastname: "SNOW",
			})
			users = append(users, types.User{
				ID: 3,
				Firstname: "Jacky",
				Lastname: "TUNING",
			})
			/*
			return []types.User{
				types.User{
					ID: 1,
					Firstname: "Hoang",
					Lastname: "CORNU",
				},
				types.User{
					ID: 2,
					Firstname: "John",
					Lastname: "SNOW",
				},
			}, nil
			*/
			return users, nil
		},
	}
}