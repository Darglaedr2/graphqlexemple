GraphQL-Go Project Example
==========================

This is base on repository `eko/graphql-go-structure`, dockerfile has been added and few users and roles

See Explanation at `https://blog.eleven-labs.com/fr/construire-structurer-api-graphql-go/`

To build `docker build -t darglaedr/graphqlexemple .`

To run `docker run -p 8080:8080 darglaedr/graphqlexemple`

To call with HTTP REST `http://localhost:8080/?query={users {firstname}}`