package types

import (
	"github.com/graphql-go/graphql"
)

// User type definition.
type User struct {
	ID        int    `db:"id" json:"id"`
	Firstname string `db:"firstname" json:"firstname"`
	Lastname  string `db:"lastname" json:"lastname"`
}

// UserType is the GraphQL schema for the user type.
var UserType = graphql.NewObject(graphql.ObjectConfig{
	Name: "User",
	Fields: graphql.Fields{
		"id":        &graphql.Field{Type: graphql.Int},
		"firstname": &graphql.Field{Type: graphql.String},
		"lastname":  &graphql.Field{Type: graphql.String},

		"roles": &graphql.Field{
			Type: graphql.NewList(RoleType),
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				var roles []Role
				userID := params.Source.(User).ID
				if userID == 1 {
					roles = append(roles, Role{
						ID:        1,
						Name: "Apprenti",

					})
					roles = append(roles, Role{
						ID:        1,
						Name: "Testeur",

					})
				} else if userID == 2 {
					roles = append(roles, Role{
						ID:        1,
						Name: "King of the north",
					})
				} else {
					roles = append(roles, Role{
						ID:        1,
						Name: "Chomeur",
					})
				}
				// userID := params.Source.(User).ID
				// Implement logic to retrieve user associated roles from user id here.

				return roles, nil
			},
		},
	},
})
