# STEP 1 build executable binary
FROM golang as builder

COPY . $GOPATH/src/graphql/exemple/
WORKDIR $GOPATH/src/graphql/exemple/

ENV GO111MODULE=on

#get dependancies
#you can also use dep
RUN go mod download

#build the binary
RUN GOOS=linux GOARCH=arm64 go build -o /go/bin/app

# STEP 2 build a small image
# start from scratch
FROM scratch
# Copy our static executable
COPY --from=builder /go/bin/app /app

EXPOSE 8080

ENTRYPOINT ["/app"]